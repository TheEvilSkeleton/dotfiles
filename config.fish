set -p PATH $HOME/.config/bubblewrap /var/lib/flatpak/exports/bin $HOME/.local/share/bin

alias flatpak-external-data-checker "flatpak run org.flathub.flatpak-external-data-checker"
